<?php
//123
class AdmisionController extends BaseController {

	//als�dfjksl�adjfsl�akfjl� daniel
	 public function getAdmisionimportar(){
	 //hola
	 	$title='Importar';
	 	return View::make('estudiante.admisionImportar')->with(array(
			'title' => $title
			));

	 }

	 public function postAjaxBuscarCedula(){
	 	if(Request::ajax()){
	 		$cedula = Input::get('cedula');


	 		
	 		$preins_complete = DB::select('SELECT nombres, apellidos, cedula, lapso, numplanilla, fecha_solicitud, numsobre, tipo_solicitud, carrera_cursar  FROM preinscritos WHERE cedula=?',array($cedula));
	 		
	 		$escuelaout= array();
	 		
	 		foreach ($preins_complete as $value) {
	 			$escuela = $value->carrera_cursar;
	 			$escuela2 = nombreEscuela($escuela);
	 			$escuelaout[] = $escuela2;
	 			$solicitud = $value->tipo_solicitud;
	 			$solicitud2[] = nombre_solicitud($solicitud);
	 			$solicitud_nom = $solicitud2;

	 		
	 			$existe= DB::connection('local')->table('estudiantes')
				->where('cedula',$cedula)
				->where('lapso',$value->lapso)
				->distinct()
				->get();
	 		}
			

	 	}
	 	return Response::json(array('preins_complete'=>$preins_complete, 'escuela'=>$escuela, 'escuela2'=>$escuela2,'escuelaout'=>$escuelaout,'solicitud_nom'=>$solicitud_nom, 'existe'=>$existe));
	 }
	 
	 public function postAjaxTrasladarPreinscrito(){
		setlocale(LC_TIME, 'es_VE'); # Localiza en espa�ol es_Venezuela
	    date_default_timezone_set('America/Caracas');
		if (Request::ajax()) {

			$cedula = Input::get('cedula');
	 		$lapso = Input::get('lap');
	 		$num = Input::get('num');
	 		$fecha_s = Input::get('fecha_s');

			$ban = 0;
			$mensaje_error = '';
			$procesado = false;

			//SQL para consultar si existe como estudiante
			$sql_estudiante = DB::connection('local')->table('estudiantes')
				->where('cedula','=',$cedula)
				->count();

			$lapso_activo = $lapso;

			$veri_lapso = $lapso_activo;

			if ($sql_estudiante != 0) {
				$mensaje_error = '<div class="alert alert-danger error-msg text-center">El estudiante ya existe en la base de datos</div>';
				$ban = 1;
			}else{
				//SQL para consultar en la tabla de preinscrito
				$sql_preinscrito = DB::table('preinscritos')
					->where('cedula',$cedula)
					->where('fecha_solicitud',$fecha_s)
					->where('numplanilla',$num)
					->where('lapso',$lapso)
					->first();
				

				
				$cod_reg = $sql_preinscrito->cod_reg;
				$cod_nuc = $sql_preinscrito->cod_nucleo;
				$cod_esc = $sql_preinscrito->carrera_cursar;
				
						
				if ($sql_preinscrito) {
						$cedula = $sql_preinscrito->cedula;
						$nom_ape = mayuscula($sql_preinscrito->apellidos).', '.mayuscula($sql_preinscrito->nombres);
						if($veri_lapso == $sql_preinscrito->lapso){
							$lapso = $sql_preinscrito->lapso;
						}else{
							$lapso = $veri_lapso;
						}
						$cod_reg = $sql_preinscrito->cod_reg;
						$nivel_est = 0;
						$cod_nuc = $sql_preinscrito->cod_nucleo;
						$veri_cod_origen = explode('-', $sql_preinscrito->numsobre);
						if($veri_cod_origen[0] == '-'){
							$mensaje_error = '<div class="alert alert-danger error-msg text-center">El N� de sobre ingresado en la web es invalido, por favor modifiquelo e intente nuevamente</div>';
							$ban = 1;
						}elseif($veri_cod_origen[0] == '00'){
							$cod_origen = 0;
						}else{
							$cod_origen = $veri_cod_origen[0];
						}
							
						$cod_men = 0;
						if ($sql_preinscrito->tipo_solicitud == 3) {
							$cod_pen = 3;
							$equivalencia = 1;
						}else{
							//SQL para verificar el pensum
							$veri_pensum = traer_pensum($sql_preinscrito->carrera_cursar);
							$equivalencia = 0;
							$cod_pen = $veri_pensum;
						}
						
						if(!empty($sql_preinscrito->carrera_cursar)){
							$cod_esc = $sql_preinscrito->carrera_cursar;
						}else{
							$mensaje_error = '<div class="alert alert-danger error-msg text-center">La carrera a cursar no puede ser Vac&iacute;a, por favor verifique</div>';
							$ban = 1;
						}
						
						$pais_nac = trim($sql_preinscrito->pais_nac);
						$edo_nac = trim($sql_preinscrito->estado_nac);
						$ciu_nac = trim($sql_preinscrito->ciudad_nac);
						$sexo = $sql_preinscrito->sexo;
						$fec_nac = date_format(date_create($sql_preinscrito->fecha_nac),'Y-m-d');
						$edo_civil = $sql_preinscrito->edo_civil;
						$dir_hab = $sql_preinscrito->direccion_per;
						$dir_res = $sql_preinscrito->direccion_res;
						$cod_res = $sql_preinscrito->codtelf_res;
						$tel_res = $sql_preinscrito->telf_res;
						$cod_hab = $sql_preinscrito->codtelf_per;
						$turno = $sql_preinscrito->turno_carrera_cursar;
						$sobre = $sql_preinscrito->numsobre;
						$tel_hab = $sql_preinscrito->telf_per;
						$tel_mov = $sql_preinscrito->cod_celular.'-'.$sql_preinscrito->celular;
						$email = trim($sql_preinscrito->email);
						
						switch ($sql_preinscrito->trabaja) {
							case 1:
								$trabaja = 'S';
							break;
							case 2:
								$trabaja = 'N';
							break;
						}
						
						$ext_nac = mayuscula($sql_preinscrito->nacionalidad);
						
						switch ($sql_preinscrito->vehiculo) {
							case 1:
								$vehiculo = 'S';
								$placa = $sql_preinscrito->placa_veh;
							break;
							case 2:
								$vehiculo = 'N';
								$placa = NULL;
							break;
						}
						
						if($sql_preinscrito->estudios_superior == 1 && $sql_preinscrito->graduado_superior == 1 && $sql_preinscrito->nivel_superior == 1){
							$proce = 1;
							$inst_origen = $sql_preinscrito->instituto_superior;
							$car_cursada = $sql_preinscrito->carrera_superior;
							$CULMINO = 1;
						}elseif ($sql_preinscrito->estudios_superior == 1 && $sql_preinscrito->graduado_superior == 1 && $sql_preinscrito->nivel_superior == 2) {
							$proce = 2;
							$inst_origen = $sql_preinscrito->instituto_superior;
							$car_cursada = $sql_preinscrito->carrera_superior;
							$CULMINO = 1;
						}elseif($sql_preinscrito->estudios_superior == 1 && $sql_preinscrito->graduado_superior == 0){
							$proce = 3;
							$inst_origen = $sql_preinscrito->instituto_superior;
							$car_cursada = $sql_preinscrito->carrera_superior;
							$CULMINO = 0;
						}else{
							$proce = 0;
							$inst_origen = '';
							$car_cursada = '';
							$CULMINO = 0;
						}
						
						$lic_ori = $sql_preinscrito->nombre_institucion_bach;
						
						if ($sql_preinscrito->tipo_institucion_bach == 1){
							$pro_lic = 1;
						}elseif ($sql_preinscrito->tipo_institucion_bach == 2) {
							$pro_lic = 0;
						}elseif ($sql_preinscrito->tipo_institucion_bach == 3) {
							$pro_lic = 3;
						}
						
						$promedio = $sql_preinscrito->promedio_bach;
						
						if (!is_null($sql_preinscrito->cedula_rep)) {
							$cedula_r = $sql_preinscrito->cedula_rep;
							$nom_ape_r = $sql_preinscrito->apellidos_rep.','. $sql_preinscrito->nombres_rep;
							$tel_mov_r = $sql_preinscrito->codtelf_rep.'-'. $sql_preinscrito->telf_rep;
						}else{
							$cedula_r = Null;
							$nom_ape_r = Null;
							$tel_mov_r = Null;
						}
						
						$nro_rusnies = $sql_preinscrito->rusnies;
						$indice_ania = 14;
						$twitter = $sql_preinscrito->twitter;
						$facebook = $sql_preinscrito->facebook;
						$men_cursada = '';





						
				}else{
					$mensaje_error = '<div class="alert alert-danger error-msg text-center">Ocurrio un error: El estudiante no tiene preinscripci&oacute;n</div>';
					$ban = 1;
				}

				if ($ban == 0) {
					//SQL para realizar el insert a las tablas estudiante, est_nvo_ingreso,carnet,numeros
					$sql_estudiante=DB::connection('local')->table('estudiantes')
						->insert(array(
							'nivel_est' => $nivel_est,
							'cedula' => $cedula,
							'cod_esc' => $cod_esc,
							'cod_pen' => $cod_pen,
							'cod_men' => $cod_men,
							'cod_nuc' => $cod_nuc,
							'cod_reg' => $cod_reg,
							'lapso' => $lapso,
							'turno' => $turno,
							'nom_ape' => $nom_ape,
							'sexo' => $sexo,
							'fec_nac' => $fec_nac,
							'pais_nac' => $pais_nac,
							'edo_nac' => $edo_nac,
							'ciu_nac' => $ciu_nac,
							'cod_origen' => $cod_origen,
							'ext_nac' => $ext_nac,
							'edo_civil' => $edo_civil,
							'dir_hab' => $dir_hab,
							'cod_hab' => $cod_hab,
							'tel_hab' => $tel_hab,
							'dir_res' => $dir_res,
							'cod_res' => $cod_res,
							'tel_res' => $tel_res,
							'vehiculo' => $vehiculo,
							'placa' => $placa,
							'tel_mov' => $tel_mov,
							'email' => $email,
							'trabaja' => $trabaja,
							'proce' => $proce,
							'lic_ori' => $lic_ori,
							'pro_lic' => $pro_lic,
							'promedio' => $promedio,
							'cedula_r' => $cedula_r,
							'nom_ape_r' => $nom_ape_r,
							'tel_mov_r' => $tel_mov_r,
							'pais' => buscarpais($pais_nac),
							'edo' => buscarestado($edo_nac,$pais_nac),
							'ciu' => buscarciudad($edo_nac,$ciu_nac),
							'condicion' => 0,
							'indice' => '0,00',
							'indice_ania' => $indice_ania,
							'estatus_activo' => 1,
							'equivalencia' => $equivalencia,
							'inst_origen' => $inst_origen,
							'car_cursada' => $car_cursada,
							'men_cursada' => $men_cursada,
							'CULMINO' => $CULMINO,
							'sobre' => $sobre,
							'discapacidad' => 0,
							'twitter' => $twitter,
							'facebook' => $facebook,
					));

					$fecha = date('Y-m-d H:i:s');

					//rutina para guardar el movimiento del estudiante
					$grabar_movi = graba_movimiento2($cedula,$cod_esc,$lapso,$cod_nuc,$cod_pen,$cod_reg,1,$fecha,0," ",0,"Traslado WEB Nuevo Ingreso",$lapso,$cod_men,0,$proce);

					//SQl para actualizar el estatus del estudiante a preinscrito para saber si ya fue trasladado
					$sql_act_pre = DB::update('UPDATE preinscritos SET estatus = 1,fecha_recibido=? WHERE cedula = ? AND cod_nucleo=? AND cod_reg = ? AND lapso=? AND carrera_cursar = ?',array(date('Y-m-d H:i:s'),$cedula,$cod_nuc,$cod_reg,$lapso,$cod_esc));

					//SQL para guardarlo en archivo original
					$sql_arch = DB::connection('local')->table('archivos_originales_status')
						->select('cedula')
						->where('cedula','=',$cedula)
						->first();

					if(count($sql_arch) == 0){
						$veri = NULL;
					}else{
						$veri = $sql_arch->cedula;
					}					

					if (is_null($veri)) {
						$sql_i = DB::connection('local')->table('archivos_originales_status')
							->insert(array(
								'cedula' => $cedula,
								'cod_status' => 1,
								'descripcion' => 'Bachiller Activo',
								'lapso' => $lapso,
						));
					}else{
						$sql_i = DB::connection('local')->update('UPDATE archivos_originales_status SET cod_status = 1 WHERE cedula=?',array($cedula));
					}


				}	
				


				if($sql_estudiante == true && $sql_i == true && $sql_i == true){
					$mensaje_error = '<div class="alert alert-success success-msg text-center">Se ha a�adido correctamente los datos del estudiante</div>';
					$procesado = true;

					$cedula_u = Session::get('cedula');
					$usuario = Session::get('usuario');
					$descripcion = 'Traslado al estudiante: '.$cedula.' a la tabla estudiante en el lapso: '.$lapso;

					auditoria($cedula_u,$usuario,$descripcion,$lapso);
				}else{
					$mensaje_error = '<div class="alert alert-danger error-msg text-center">Ha ocurrido un error. Por favor verifique con el departamento de inform&aacute;tica</div>';
					$procesado = false;
				}
			}
		}
		return Response::json(array(
			'mensaje_error' => $mensaje_error,
			'procesado' => $procesado
		));
	}

	 
}
